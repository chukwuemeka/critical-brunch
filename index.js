"use strict";

const critical = require("critical");
const request = require("request-promise-native");
const path = require("path");

class Critical {
  constructor(config) {
    this.config = config || {};
  }
  onCompile(files, assets) {
    if (!this.config || !this.config.plugins || !this.config.plugins.critical) {
      return;
    }
    const config = this.config;
    const jobs = config.plugins.critical.jobs;
    jobs.forEach(function(job) {
        const destPath = config.paths.public;
        const options = Object.assign({}, job);
        const src = options.src;
        delete options.src;
        request.get(src).then(function(response) {
          const params = Object.assign({}, options, {
            html: response,
            inline: false
          });
          critical.generate(params);
        });
    });
  }
}

Critical.prototype.brunchPlugin = true;
Critical.prototype.defaultEnv = "production";

module.exports = Critical;
